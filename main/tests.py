from django.test import TestCase
from django.contrib.auth.models import User, Group
from main.models import Role, UserRole


class RoleTest(TestCase):
	user = role = None

	def setUp(self):
		self.user = User.objects.create(username="elonmusk", password="tesla")
		self.role = Role.objects.create(name='ceo')

	def test_save_role(self):
		'''Group object should be created when a role is saved.'''
		self.assertTrue(Group.objects.filter(name='ceo'))

	def test_attach_user_to_role(self):
		'''Confirm User links to Role and Group.'''
		group = Group.objects.filter(name='ceo')
		users = User.objects.filter(groups__name=group[0].name)
		self.assertFalse(users)
		self.assertFalse(UserRole.objects.filter(name='elonmusk-ceo'))
		self.role.attach_user_to_role(self.user)
		users = User.objects.filter(groups__name=group[0].name)
		self.assertTrue(users)
		self.assertEqual(users[0].username, 'elonmusk')
		self.assertTrue(UserRole.objects.get(name='elonmusk-ceo'))
		

	def test_remove_user_from_role(self):
		'''Confirm User unlinks from Role and Group.'''
		group = Group.objects.filter(name='ceo')
		self.role.attach_user_to_role(self.user)
		users = User.objects.filter(groups__name=group[0].name)
		self.assertTrue(users)
		self.assertTrue(UserRole.objects.get(name='elonmusk-ceo'))
		self.assertEqual(users[0].username, 'elonmusk')
		self.role.remove_user_from_role(self.user)
		users = User.objects.filter(groups__name=group[0].name)
		self.assertFalse(UserRole.objects.filter(name='elonmusk-ceo'))
		self.assertFalse(users)

	def test_attach_users_to_role(self):
		'''Confirm Users links to Role and Group.'''
		pass

	def test_remove_users_from_role(self):
		'''Confirm Users unlinks from Role and Group.'''
		pass

