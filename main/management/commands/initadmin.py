from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            for user in settings.ADMINS:
                username = user[0].replace(' ', '')
                email = user[1]
                password = 'admin'
                self.stdout.write('Creating Admin "%s"' % username)
                admin = User.objects.create_superuser(email=email, username=username, password=password)
                admin.is_active = True
                admin.is_admin = True
                print(admin)
                admin.save()
                self.stdout.write(self.style.SUCCESS('Successfully created admin "%s"' % username))
        else:
            print('Admin users can only be initialized if no User exist')