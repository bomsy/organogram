from django.core import serializers



def get_recursive_roles_data(queryset):
	'''Returns nested tree data for roles.'''
	def set_children(context, root):
		bits = []
		c = {}
		for child in root.get_children():
			bits.append(set_children(context,child))
		c['name'] = root.name
		c['children'] = bits
		return c
	if not queryset:
		return []
	root = queryset[0]
	c = set_children({},root)
	return [c]

def get_recursive_users_data(queryset):
	'''Return nested tree data for users.'''
	def set_children(context, root):
		bits = []
		c = {}
		for child in root.get_children():
			bits.append(set_children(context,child))
		c['name'] = root.user.username
		c['role'] = root.role.name
		c['user'] = root.user.username
		c['children'] = bits
		return c
	if not queryset:
		return []
	root = queryset[0]
	c = set_children({},root)
	return [c]



