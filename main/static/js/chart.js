
var thumbSize,
thumbTooltipScale,
toolTipDuration,
thumbHeight,
thumbWidth,
linkLength,
thumbTooltipWidth,
thumbTooltipHeight;

function calculate_sizes(){
  thumbSize = 50;
  thumbTooltipScale = 3;
  toolTipDuration = 400;
  thumbHeight = thumbSize;
  thumbWidth = thumbSize;
  linkLength = thumbSize+100;
  thumbTooltipWidth = 500;
  thumbTooltipHeight = 250;

}


var treeData1 = [
  {
    "name": "Top Level",
    "parent": "null",
    "children": [
      {
        "name": "Level 2: A",
        "parent": "Top Level",
        "children": [
          {
            "name": "Son of A",
            "parent": "Level 2: A",
            "children":[{
              "name": "Level 2: A1",
              "parent": "Son of A",
              "children":[
                {
                  "name":"dfhgdfg",
                  "parent":"Level 2: A1",
                  "children":[
                    {
                      "name":"boms",
                      "parent":"dwifgh"
                    }
                  ]
                }
              ]
            }]
          },
          {
            "name": "Daughter of A",
            "parent": "Level 2: A"
          }
        ]
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level",
        "children":[
          {
             "name": "Daughter of A",
              "parent": "Level 2: B"
          },
          {
            "name": "Daughter of A",
              "parent": "Level 2: B"
          }
        ]
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },
      {
        "name": "Level 2: B",
        "parent": "Top Level"
      },

    ]
  }
];



// ************** Generate the tree diagram  *****************

var draw_org_chart = function(container_id, treeData){

	if(treeData == null || treeData == undefined){
		return
	}

	calculate_sizes();

	$("#"+container_id).html('');


var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = Math.min(2500, window.innerWidth) - margin.right - margin.left,
    height =Math.min(2500, window.innerHeight) - margin.top - margin.bottom;
    
var i = 0,
    duration = 500,
    root;

var tree = d3.layout.tree()
    .size([height, width]);


var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.x, d.y]; });

var svg = d3.select("#"+container_id).append("svg")
  .append("g");

var  container = d3.select("#"+container_id).append("div")
    .attr("class","cont");

root = treeData[0];
root.x0 = width / 2;
root.y0 = 0;

update(root);


//UnComment this block to start from single unexpanded unit
// $.each(tree.nodes(root).reverse(),function(i,d){
//           if (d.children) {
//           d._children = d.children;
//           d.children = null;
//         } else {
//           d.children = d._children;
//           d._children = null;
//         }
//         update(d);
//   });
  


d3.select(self.frameElement).style("height", "500px");


function update(source) {
  var depth = 0;
  $.each(tree.nodes(root).reverse(),function(i,v){
    depth = Math.max(depth,v.depth);
  });
  var newHeight = Math.max(depth * linkLength, height);
  var newWidth = Math.max(linkLength, width);

  var pos = Math.max(thumbHeight/2,(window.innerHeight/2) - ((depth * linkLength)/2))

  d3.select("#"+container_id+" .cont")
  .style("width", width + margin.right + margin.left+"px")
  .style("height", (newHeight - pos)+ margin.top + margin.bottom +"px")
  .transition()
  .duration(duration)
  .style("top",function(d){return pos+"px";})
  .style("left",function(d){return "5px";});

  d3.select("#"+container_id+" svg")
  .style("width", width + margin.right + margin.left+"px")
  .style("height", (newHeight- pos) + margin.top + margin.bottom +"px")
  .transition()
  .duration(duration)
  .attr("transform", function(d) { return "translate(" + 5 + "," + pos + ")"; });

  d3.select("#"+container_id+" svg g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  tree = d3.layout.tree().size([width, newHeight]);

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * linkLength; });

  // Update the nodes…
  var node = container.selectAll(".node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("div")
      .style("min-width", thumbWidth+"px")
      .style("height", thumbHeight+"px")
      .style("padding", "8px")
      .style("background","#eee")
      .style("position","absolute")
      .style("transform", "translate(" + margin.left + "px," + margin.top + "px)")
      .style("top",(source.y0-(thumbHeight/2)+"px"))
      .style("left",(source.x0-(thumbWidth/2)+"px"))
      .attr("class", "node")
      .on("click", click)
      .append("p")
      .text(function(d){return d.name;})
      .style("text-align","center");
      // .on("mouseenter", handleMouseOver)
      // .on("mouseleave", handleMouseLeave)
      // .append("img")
      // .attr("src","https://picsum.photos/100/100/?random");

  nodeEnter.append("p")
  .text(function(d){return d.role;})
  .style("text-align","center")
  .style("font-size","10px");

  //indicator for endnode
  nodeEnter.append("div")
  .style("position", "absolute")
  .style("height","3px")
  .style("bottom","0px" )
  .style("left","0px" )
  .style("width","100%")
  .style("background", function(d){return d.endNode?"#eee":"green";});

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .style("top",function(d){return (d.y-(thumbHeight/2))+"px";})
      .style("left",function(d){return (d.x-(thumbWidth/2))+"px";});




  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .style("top",function(d){return (source.y-(thumbHeight/2))+"px";})
      .style("left",function(d){return (source.x-(thumbWidth/2))+"px";})
      .remove();

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });


  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
    $.each(d._children?d._children:d.children, function(i,v){
      v.endNode = v._children?false:true;
    });
  update(d);
}

//Mousee over event
// function handleMouseOver(d){
//   var s = d3.select(this).select(".thumbTooltip");
//   if(s.empty()){
//       d3.select(this).insert("div")
//     .attr("class","thumbTooltip")
//     .style("height", thumbHeight+"px")
//     .style("width", thumbWidth+"px")
//     .style("position","absolute")
//     .style("border", "1px solid #222")
//     .style("bottom","0px")
//     .style("left","0px")
//     .style("box-shadow", "0px 0px 0px 0px #bbb")
//     .transition()
//     .duration(toolTipDuration)
//     .style("bottom",""+(thumbHeight+10)+"px")
//     .style("left","-"+((thumbTooltipWidth/2)-(thumbWidth/2))+"px")
//     .style("width",thumbTooltipWidth+"px")
//     .style("height",thumbTooltipHeight+"px")
//   }

// }

// function handleMouseLeave(d){
//   d3.select(this).select(".thumbTooltip")
//   .transition()
//   .duration(toolTipDuration)
//   .style("width",thumbWidth+"px")
//   .style("height",thumbHeight+"px")
//   .style("bottom","0px")
//   .style("left","0px")
//   .remove();
// }

$(window).resize(function(){
    draw_org_chart(container_id, treeData);
});

}





