
// Get CSRF Token from template
var csrf_token = $("input[name='csrfmiddlewaretoken']").val();

// data urls located in hidden input values 
// from template
var add_role_url = $("input[name='add_role_url']").val();
var delete_role_url = $("input[name='delete_role_url']").val();
var link_user_role_url = $("input[name='link_user_role_url']").val();
var unlink_user_role_url = $("input[name='unlink_user_role_url']").val();
var users_chart_data_url = $("input[name='users_chart_data_url']").val();
var roles_chart_data_url = $("input[name='roles_chart_data_url']").val();

function Organogram(){};

Organogram.add_role = function(role, parent, callback){
	// Add a role to the DB
	// Permissions will be checked at the backend
	$.ajax({
		url:add_role_url,
		method:'post',
		data:{'role_name':role, 'parent_role':parent,
				'csrfmiddlewaretoken':csrf_token},
		dataType:"json",
		success:function(r){
			if(callback){
				callback(r);
			}
		},
		error:function(r){
			if(callback){
				callback(r);
			}
		}
	});
}

Organogram.remove_role = function(role, callback){
	// Remove role
	// Permissions will be checked at the backend
	$.ajax({
		url:delete_role_url,
		method:'post',
		data:{'role_name':role, 'csrfmiddlewaretoken':csrf_token},
		dataType:"json",
		success:function(r){
			if(callback){
				callback(r);
			}
		},
		error:function(r){
			if(callback){
				callback(r);
			}
		}
	});
}

Organogram.attach_user_to_role = function(role, username, callback){
	// Link user to role
	// Permissions will be checked at the backend
	$.ajax({
		url:link_user_role_url,
		method:'post',
		data:{'role_name':role,
			'username':username,
			'csrfmiddlewaretoken':csrf_token},
		dataType:"json",
		success:function(r){
			if(callback){
				callback(r);
			}
		},
		error:function(r){
			if(callback){
				callback(r);
			}
		}
	});
}

Organogram.remove_user_from_role = function(role, username, callback){
	// Unlink user from role
	// Permissions will be checked at the backend
	$.ajax({
		url:unlink_user_role_url,
		method:'post',
		data:{'role_name':role,
			'username':username,
			'csrfmiddlewaretoken':csrf_token},
		dataType:"json",
		success:function(r){
			if(callback){
				callback(r)
			}
		},
		error:function(r){
			if(callback){
				callback(r);
			}
		}
	});
}


Organogram.render_users_chart = function(root, callback){
	// Provide User data for the chart
	// Leave 'root' as null to retrieve all data
	$.ajax({
		url:users_chart_data_url,
		method:'get',
		data:{'root':root},
		dataType:"json",
		success:function(r){
			if(callback){
				callback(r);
			}

		},
		error:function(r){
			if(callback){
				callback(r);
			}
		}
	});

}

Organogram.render_roles_chart = function(root, callback){
	// Provide Role data for the chart
	// Leave 'root' as null to retrieve all data
	$.ajax({
		url:roles_chart_data_url,
		method:'get',
		data:{'root':root},
		dataType:"json",
		success:function(r){
			// console.log(r);
			callback(r);
		},
		error:function(r){
			console.log(r);
			if(callback){
				callback(r);
			}
		}
	});


}

Organogram.render_chart = function(container_id, flag){
	// Render the Organogram chart in the container element
	// 
	// :container_id = id of existing container element(most likely a 'div')

	if(flag == "roles" || flag == undefined){
		Organogram.render_roles_chart(null,function(data){
		draw_org_chart(container_id, data);
	});
	}else if(flag == "users"){
		Organogram.render_users_chart(null,function(data){
			draw_org_chart(container_id, data);
		});
	}
}

// *********************Sample Data***************



// Run Sample Data
sampledata = []
sampledata.push(['','Ceo', 'VP Marketing','Sales Manager','Sales1']);
sampledata.push(['','Ceo', 'VP Marketing','Sales Manager','Sales2']);
sampledata.push(['','Ceo', 'VP Marketing','Advertising Manager']);
sampledata.push(['','Ceo', 'VP Finance','Chief Accountant','Accountant']);
sampledata.push(['','Ceo', 'VP Manufacturing','Plant Manager','Plant Supervisor']);
sampledata.push(['','Ceo', 'VP Manufacturing','Maintenance Head']);
sampledata.push(['','Ceo', 'VP HR','HR Manager','Recruiter']);


function add_sample_data(data){
	if(data.length > 1){
		Organogram.add_role(data[1],data[0], function(response){
			data.shift();
			add_sample_data(data);
		});
	}else{
			// Render Sample test chart for roles
			// The template already has a div with id 'org-chart'
			Organogram.render_chart("org-chart", "roles");
	}
}

$.each(sampledata, function(i,v){
	add_sample_data(v);
});

