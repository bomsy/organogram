from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required,permission_required
from django.shortcuts import get_object_or_404
from django.core import serializers
from django.contrib.auth.models import User, Group
from django.http import JsonResponse, HttpResponse
from main.models import Role, UserRole
from main.utils import (get_recursive_roles_data,get_recursive_users_data)


def home(request, *args, **kwargs):
	return render(request, "home.html", {})

def chart(request, *args, **kwargs):
	return render(request, "chart.html", {})


def get_all_roles(request, *args, **kwargs):
	'''Return all roles for ajax requests.'''
	family = None
	root = request.GET.get('root')
	if root:
		root = Role.objects.filter(name=root)
		if root:
			family = root.get_descendants(include_self=True)
	if not family:
		family = Role.objects.all()
	family = get_recursive_roles_data(family)
	# print(family)
	return JsonResponse(family, safe=False)

def get_all_users(request, *args, **kwargs):
	'''Return all users and roles for ajax requests.'''
	family = None
	root = request.GET.get('root')
	if root:
		root = UserRole.objects.filter(username=root)
		if root:
			family = root.get_descendants(include_self=True)
	if not family:
		family = UserRole.objects.all()
	family = get_recursive_users_data(family)
	return JsonResponse(family, safe=False)

@login_required
@permission_required('main.add_group', raise_exception=True)
def add_role(request, *args, **kwargs):
	'''Add Role.'''
	created = role = None
	if request.method == "POST":
		role_name = request.POST.get('role_name')
		parent = request.POST.get('parent_role') or None
		if parent:
			parent = Role.objects.filter(name=parent)
			if not parent:
				return JsonResponse({'role':role, 'created':created})
			parent = parent[0]
		if role_name:
			role, created = Role.objects.get_or_create(name=role_name, parent=parent)
	if role:
		role = serializers.serialize('json', [ role, ])
	return JsonResponse({'role':role, 'created':created})

@login_required
@permission_required('main.delete_group', raise_exception=True)
def delete_role(request, *args, **kwargs):
	'''Remove Role.'''
	deleted = None
	if request.method == "POST":
		role_name = request.POST.get('role_name')
		if role_name:
			role = Role.objects.filter(name=role_name)
			if role:
				role[0].delete()
				deleted = True
	return JsonResponse({'deleted':deleted})

@login_required
@permission_required('main.change_group', raise_exception=True)
@permission_required('main.add_group', raise_exception=True)
def link_user_role(request, *args, **kwargs):
	'''Link user to Group.'''
	role = username = None
	if request.method == 'POST':
		role = request.POST.get('role_name')
		username = request.POST.get('username')
	if any([not role, not username ]):
		return JsonResponse({'linked':None})
	role = Role.objects.filter(name=role)
	if role:
		role[0].attach_user_to_role(username)
		return JsonResponse({'linked':True})
	return JsonResponse({'linked':False})


@login_required
@permission_required('main.change_group', raise_exception=True)
@permission_required('main.add_group', raise_exception=True)
def unlink_user_role(request, *args, **kwargs):
	'''Unlink user from Group.'''
	role = username = None
	if request.method == 'POST':
		role = request.POST.get('role_name')
		username = request.POST.get('username')
	if any([not role, not username ]):
		return JsonResponse({'unlinked':None})
	role = Role.objects.filter(name=role)
	if role:
		role[0].remove_user_from_role(username)
		return JsonResponse({'unlinked':True})
	return JsonResponse({'unlinked':False})



