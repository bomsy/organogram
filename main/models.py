from django.db import models
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User, Group
from mptt.models import MPTTModel, TreeForeignKey
import mptt


class Role(MPTTModel):
	name = models.CharField(max_length=100, unique=True)
	parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True,blank=True)
	group = models.OneToOneField(Group, on_delete=models.CASCADE, null=True,blank=True)

	class MPTTMeta:
		order_insertion_by = ['name']
		# unique_together = (('name','parent'),)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		if not self.group:
			self.group = Group.objects.create(name=self.name)
		super().save(*args, **kwargs)

	def attach_user_to_role(self, user):
		'''Link user to Role and Group.'''
		if isinstance(user, str):
			user = get_object_or_404(User, username=user)
		group, created = Group.objects.get_or_create(name=self.group)
		group.user_set.add(user)
		group.save()
		UserRole.objects.create(user=user, role=self)

	def remove_user_from_role(self, user):
		'''Unlink user from Role and Group.'''
		if isinstance(user, str):
			user = get_object_or_404(User, username=user)
		group, created = Group.objects.get_or_create(name=self.group)
		users = User.objects.filter(groups__name=group.name)
		if user in users:
			group.user_set.remove(user)
			group.save()
		userrole = UserRole.objects.filter(user=user,role=self)
		if userrole:
			userrole[0].delete()

	def attach_users_to_role(self, users):
		'''Link users to Role and Group.'''
		for user in users:
			self.attach_user_to_role(user)

	def remove_users_from_role(self, users):
		'''Unlink users from Role and Group.'''
		for user in users:
			self.remove_users_from_role(user)


class UserRole(MPTTModel):
	name = models.CharField(max_length=100, unique=True, blank=True)
	parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True,blank=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	role = models.ForeignKey(Role, on_delete=models.CASCADE)

	class MPTTMeta:
		order_insertion_by = ['name']

	def save(self, *args, **kwargs):
		if not self.name:
			self.name = "{}-{}".format(self.user.username, self.role.name)
		if not self.parent:
			# Use user's role to determine its parent user
			parent = Role.objects.filter(name=self.role.parent)
			if parent:
				urole = UserRole.objects.filter(role=parent[0])
				if urole:
					self.parent = emps[0]
		super().save(*args, **kwargs)