"""organogram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from main.views import (home, chart, get_all_users, get_all_roles,
						add_role, delete_role,unlink_user_role,
						link_user_role)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('chart/', chart),
    path('users/', get_all_users, name="users_chart_data_url"),
    path('roles/', get_all_roles, name="roles_chart_data_url"),
    path('role/add/', add_role, name="add_role_url"),
    path('role/delete/', delete_role, name="delete_role_url"),
    path('userrole/link/', link_user_role, name="link_user_role_url"),
    path('userrole/unlink/', unlink_user_role, name="unlink_user_role_url"),
]
urlpatterns += staticfiles_urlpatterns()
